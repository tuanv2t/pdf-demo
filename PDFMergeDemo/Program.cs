﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFMergeDemo
{
    class Program
    {
        static void Main(string[] args)
        {
             
            var file1 =  File.ReadAllBytes("PDF_Files/File1.pdf");
            var file2 = File.ReadAllBytes("PDF_Files/File2.pdf");
            var mergeContentByte = new List<byte[]>(2);
            mergeContentByte.Add(file1);
            mergeContentByte.Add(file2);
            var mergedResult = iTextSharpDemo.MergeFiles(mergeContentByte);
            //write to fild
            var fileName = string.Format("Merge_{0}.pdf", DateTime.Now.Ticks);
            File.WriteAllBytes(fileName, mergedResult);
        }
    }
}
